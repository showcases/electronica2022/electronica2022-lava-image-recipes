#!/bin/sh

set -e

# fail fast here if file is absent (to catch changes in upstream if any)
# NB: we generate /etc/fstab with debos, so use systemd's mechanism
cp /usr/share/systemd/tmp.mount /etc/systemd/system/
systemctl enable tmp.mount

cp /usr/share/systemd/tmp.mount /etc/systemd/system/var-tmp.mount
sed -i 's,/tmp,/var/tmp,g' /etc/systemd/system/var-tmp.mount
systemctl enable var-tmp.mount

