#!/bin/sh

set -e

distro=$1
rel=$2
timestamp=$3
vendor=$4
variant=$5

echo "${distro} ${rel} ${timestamp} ${vendor}" > /etc/image_version

if [ -n "$variant" ]; then
    echo "VARIANT_ID=$variant" >> /etc/os-release
fi
if [ -n "$timestamp" ]; then
    echo "BUILD_ID=$timestamp" >> /etc/os-release
fi
