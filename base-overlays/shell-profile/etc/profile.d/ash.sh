if [ "$(id -u)" -eq 0 ]; then
  export PS1="\u@\h:\w# "
else
  export PS1="\u@\h:\w\$ "
fi
