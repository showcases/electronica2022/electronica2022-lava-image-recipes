#!/usr/bin/env python3

from jinja2 import Environment, FileSystemLoader
import yaml
import os
import argparse

class ParseKwargs(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        key, value = values.split('=')
        if getattr(namespace, self.dest) == None:
            setattr(namespace, self.dest, dict())
        getattr(namespace, self.dest)[key] = value

parser = argparse.ArgumentParser(
                    prog = 'generate_lava_job',
                    description = 'GEeerate lava jobs')
parser.add_argument('-e', '--env', action=ParseKwargs)
parser.add_argument('--env-file', action='append')
args = parser.parse_args()

env = args.env
for file in args.env_file:
    s = open(file, 'r')
    env = env | list(yaml.load_all(s, Loader=yaml.FullLoader))[0]

jinja = Environment(loader=FileSystemLoader('.'))
template = jinja.get_template('testjob.jinja2')
data = template.render(env=env);
print(data)
